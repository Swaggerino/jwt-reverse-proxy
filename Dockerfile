FROM python:3
ADD proxy.py /
ADD requirements.txt /
RUN pip install -r requirements.txt
CMD [ "python", "./proxy.py" ]
