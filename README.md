## Description

A simple dockerized web Reverse Proxy to verify JWT signature and throw 403 errors when needed.
This was written for a school project in which we had to harden OWASP's vulnerable web app, [Juice Shop](https://owasp.org/www-project-juice-shop/), without changing any source code.

## Usage

The image can be built using the following
```bash=
docker build ./ -t jwt-proxy
```

Then we can launch the image, redirecting 8081 port to whichever we want, this can be done using the docker-compose syntax, along with other services :

```yml=
version: '3'
services:
    reverse-proxy-JWT:
        image: jwt-proxy
        container_name: reverse-proxy-jwt
        ports:
            - 8081:8081
```

## TODO

Support different signature algorithms than RS256.

## License
[MIT](https://choosealicense.com/licenses/mit/)
