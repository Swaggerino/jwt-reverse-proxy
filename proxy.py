#/usr/bin/python3

from http.server import HTTPServer, SimpleHTTPRequestHandler
import requests
import socketserver
import jwt


class JWTReverseProxy(SimpleHTTPRequestHandler):
    protocol_version = 'HTTP/1.0'
    public_key = "-----BEGIN RSA PUBLIC KEY-----\nMIGJAoGBAM3CosR73CBNcJsLv5E90NsFt6qN1uziQ484gbOoule8leXHFbyIzPQRozgEpSpiwhr6d2/c0CfZHEJ3m5tV0klxfjfM7oqjRMURnH/rmBjcETQ7qzIISZQ/iptJ3p7Gi78X5ZMhLNtDkUFU9WaGdiEb+SnC39wjErmJSfmGb7i1AgMBAAE=\n-----END RSA PUBLIC KEY-----"
    alg = "RS256"

    def do_GET(self, body=True):
        hostname = '172.17.0.1:8888'
        url = f"http://{hostname}{self.path}"# attention au https
        
        try :   
            token = self.headers.get("Cookie").split(";")
            token = [x for x in token if "token=" in x][0] # Si la liste est vide (quand le token n'est pas présent), lève une IndexError
            token = token[7:]
            token = jwt.decode(token,self.public_key,algorithms=[self.alg])
            resp = requests.get(url, headers=self.headers, verify=False)
            self.send_response(resp.status_code)
            self.send_header("Content-type", resp.headers['Content-type']) #Permet de gérer correctement le header X-Content-Type-Options: nosniff en mettant le bon type MIME
            #self.send_header("Cookie", resp.headers['Cookie'])
            self.end_headers()
            self.wfile.write(resp.content)

        except (jwt.exceptions.InvalidAlgorithmError, jwt.exceptions.InvalidSignatureError) as e: # Cas de tentative d'attaque par cookie tampering
            print("Mechante exception\n",e)
            self.send_response(403)
            self.send_header("Content-type","text/html")
            self.end_headers()
            self.wfile.write(b"<body><h1>Tu fais quoi gamin? Disposax.</h1><img src='https://cdn.dribbble.com/users/15687/screenshots/14919436/hackerman_4x.png'></body>")
     
        
        except (IndexError , jwt.exceptions.DecodeError) as e: # Cas où il n'y a pas de token, on laisse passer la requête
            print("Pas de token\n")
            resp = requests.get(url, headers=self.headers, verify=False)
            self.send_response(resp.status_code)
            self.send_header("Content-type", resp.headers['Content-type']) #Permet de passer le nosniff en mettant le bon MIME type
            self.end_headers()
            self.wfile.write(resp.content)
        
        except(AttributeError) as e :
            print("Pas de Cookie\n")
            resp = requests.get(url, headers=self.headers, verify=False)
            self.send_response(resp.status_code)
            self.send_header("Content-type", resp.headers['Content-type']) #Permet de passer le nosniff en mettant le bon MIME type
            self.end_headers()
            self.wfile.write(resp.content)
    
    def do_POST(self, body=True):
        hostname = '172.17.0.1:8888'
        url = f"http://{hostname}{self.path}"# attention au https
        content_len = int(self.headers.get('Content-Length'))
        post_body = self.rfile.read(content_len) # On récupère le contenu de la requête POST
        print(post_body)
        
        try :   
            token = self.headers.get("Cookie").split(";")
            token = [x for x in token if "token=" in x][0] # Si la liste est vide (quand le token n'est pas présent), lève une IndexError
            token = token[7:]
            token = jwt.decode(token,self.public_key,algorithms=[self.alg])
            resp = requests.post(url, headers=self.headers, verify=False, data=post_body)
            self.send_response(resp.status_code)
            self.send_header("Content-type", resp.headers['Content-type']) #Permet de passer le nosniff en mettant le bon MIME type
            self.end_headers()
            self.wfile.write(resp.content)

        except (jwt.exceptions.InvalidAlgorithmError, jwt.exceptions.InvalidSignatureError) as e: # Cas de tentative d'attaque par cookie tampering
            print("Mechante exception\n")
            self.send_response(403)
            self.send_header("Content-type","text/html")
            self.end_headers()
            self.wfile.write(b"<body><h1>Tu fais quoi gamin? Disposax.</h1><img src='https://cdn.dribbble.com/users/15687/screenshots/14919436/hackerman_4x.png'></body>")

        
        except IndexError as e: # Cas où il n'y a pas de token, on laisse passer la requête
            print("pas de token")
            resp = requests.post(url, headers=self.headers, verify=False, data=post_body)
            self.send_response(resp.status_code)
            self.send_header("Content-type", resp.headers['Content-type']) #Permet de passer le nosniff en mettant le bon MIME type
            self.end_headers()
            self.wfile.write(resp.content)

    
    

if __name__ == '__main__':
    server_address = ('0.0.0.0', 8081) #Adresse de l'interface du container
    httpd = socketserver.ForkingTCPServer(server_address, JWTReverseProxy)
    print('http server is running')
    httpd.serve_forever()
